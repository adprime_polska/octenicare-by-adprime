<?php /* Template Name: Page */ ?>
<?php get_header(); ?>

<main>
  <div class="container">
    <article class="article">

<?php
while ( have_posts() ) : the_post();
?>

<h2 class="title title--feat"><?php the_title(); ?></h2>

<?php
the_content();
endwhile;
?>

    </article>
  </div>
</main>

<?php get_footer(); ?>
