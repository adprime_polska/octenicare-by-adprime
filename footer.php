    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-6 text-left footer__left">
              Copyright © 2019 Schülke. Wszelkie prawa zastrzeżone.
              <br>
              Opiekun kampanii: <a class="footer--pink" href="//adprime.pl" target="_blank">AdPrime™</a>
              <br>
              <a class="footer--white" href="<?php echo get_permalink( get_page_by_path('polityka-prywatnosci')); ?>" target="_self">Polityka prywatności i informacje o cookies</a>
          </div>
          <div class="col-6 text-right footer__right">
            <a class="footer__link" href="//www.schuelke.com/pl-pl/" target="_self"></a>
          </div>
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
