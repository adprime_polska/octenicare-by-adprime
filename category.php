<?php /* Template Name: Category */ ?>
<?php get_header(); ?>

<main>
  <section class="mainSection" itemprop="mainEntity">
    <div class="container" itemscope itemtype="http://schema.org/ItemList">
      <h2 class="title title__section">
        <?php single_cat_title(); echo ' '; the_field('subtitle'); ?>
      </h2>
      <div class="row">
        <div class="col-4 img__featuredWrap">

<?php if (function_exists('z_taxonomy_image_url')) { ?>

          <span class="img--featured mt-3" style="background-image:url(<?php echo z_taxonomy_image_url(); ?>)"></span>

<?php } ?>

        </div>
        <div class="col-8">
          <article class="article article--single">

<?php
$content = category_description($cat_id);
$bolded = preg_replace('/<p([^>]+)?>/', '<p$1 class="lead">', $content, 1);
echo $bolded;
?>

          <article>
        </div>
      </div>
      <div class="text-center mb-5">
        <a class="btn btn-primary" href="#zastosowanie">Powrót</a>
      </div>
    </section>
  </main>

<?php get_footer(); ?>
