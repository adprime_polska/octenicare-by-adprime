<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <base href="/">
    <title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="fragment" content="!">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="shortcut icon" href="">
    <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="Kanał RSS <?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <script type="text/javascript">
      var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127681785-13"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-127681785-13');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 924065713 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-924065713"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'AW-924065713');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '986074168434602');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=986074168434602&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->

  </head>
  <body>
    <a id="top"></a>
    <header class="header">
      <div class="container">
        <div class="row">
          <div class="col-3 header__left">
          </div>
          <div class="col-6 header__center">
            <a class="header__link" href="<?php echo home_url(); ?>" target="_self"></a>
          </div>
          <div class="col-3 header__right">
            <a class="header__link" href="//www.schuelke.com/pl-pl/" target="_self"></a>
          </div>
        </div>
      </div>
      <a class="logo logo__header" href="<?php echo home_url(); ?>"></a>
      <nav class="mainNav">
        <input class="mainNav__burger" id="burger" name="burger" type="checkbox">
        <label class="mainNav__label" for="burger"></label>

<?php
$defaults = array(
'theme_location' => 'main_menu',
'menu'           => 'main-menu',
'depth'          => 2,
'container'      => false,
'menu_class' 	   => 'mainNav__container',
);
wp_nav_menu( $defaults );
?>

      </nav>
    </header>
