"use strict";

var $ = jQuery.noConflict(),
  body = $('body'),
  App = {
    init: function() {
      this.adPrimeForm();
      this.checkBoxFix();
      this.scrollNav();
      this.headerFix();
      this.myDrawers();
      this.downloadBtnFix();
    },

    // adPrime form
    adPrimeForm: function() {
      var form = $('#cf-alert');
      var submit = $('#cf-submit');
      var alert = $('#cf-alert');
      form.validate({
        ignore: ".ignore",
        rules: {
          cf_subject: {
            required: true,
            minlength: 5
          },
          cf_content: {
            required: true,
            minlength: 5
          },
          cf_email: {
            required: true,
            email: true
          },
          hiddenRecaptcha: {
            required: function() {
              if (grecaptcha.getResponse() == '') {
                return true;
              } else {
                return false;
              }
            }
          }
        },
        messages: {
          "cf_subject": {
            required: "Podaj temat wpisu",
            minlength: "Temat wpisu musi zawierać co najmniej 10 znaków"
          },
          "cf_content": {
            required: "Podaj treść wpisu",
            minlength: "Treść wpisu musi zawierać co najmniej 500 znaków"
          },
          "cf_email": {
            required: "Podaj adres e-mail",
            email: "Podaj prawidłowy adres e-mail"
          },
          "hiddenRecaptcha": {
            required: "Potwierdź, że nie jesteś robotem"
          }
        }
      });
      $('body').on('click', '#cf-submit', function(e) {
        e.preventDefault();
        if (form.valid()) {
          var fd = new FormData();
          fd.append('msg_subject', $('input[name=cf_subject]').val());
          fd.append('msg_content', $('textarea[name=cf_content]').val());
          fd.append('msg_name', $('input[name=cf_name]').val());
          fd.append('msg_email', $('input[name=cf_email]').val());
          fd.append('action', 'cvf_send_form');
          $.ajax({
            type: 'POST',
            url: 'http://octenicare.pl/wordpress/wp-admin/admin-ajax.php',
            data: fd,
            contentType: false,
            processData: false,
            beforeSend: function() {
              alert.fadeOut();
              submit.html('Wysyłam...');
            },
            success: function(response) {
              alert.html('Twoja wiadomość została wysłana.').fadeIn();
              form.trigger('reset');
              submit.html('Wyślij');
            },
            error: function(e) {
              console.log(e)
            }
          });
        }
      });
    },

    // cf7 checkbox fix
    checkBoxFix: function() {
      var checkbox = $(".checkbox").find("input[type=checkbox]");
      checkbox.each(function(index) {
        var val = $(this).val();
        var id = $(this)[0].name;
        $(this).attr('id', id);
        $(this).after("<label for=" + id + ">" + val + "</label>");
      });
    },

    // smooth scroll nav
    scrollNav: function() {
      var scrollTo = function(element) {
        if (window.innerWidth < 768) {
          $('html,body').stop().animate({
            scrollTop: element.offset().top - 90
          }, 500, 'easeInOutExpo');
        }
        if (window.innerWidth > 768) {
          $('html,body').stop().animate({
            scrollTop: element.offset().top
          }, 500, 'easeInOutExpo');
        }
      };
      $('a[href*=#]:not([href=#])').click(function() {
        var $anchor = $(this);
        $anchor.addClass('active').parent('li').siblings().children('a').removeClass('active');
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            scrollTo(target);
            return false;
          };
        }
      });
      $(window).scroll(function() {
        var windscroll = $(window).scrollTop();
        $('section').each(function(i) {
          if ($(this).position().top <= windscroll + 5) {
            $('nav ul li.menu-item a').removeClass('active');
            $('nav ul li.menu-item a').eq(i).addClass('active');
          }
        });
      }).scroll();
    },

    // header fix
    headerFix: function() {
      var yOffset = $(".header").offset().top;
      $(window).scroll(function() {
        if (($(window).scrollTop() > yOffset + 200) && (window.innerWidth < 768)) {
          $(".mainNav").css({
            'position': 'fixed',
            'top': '0',
            'left': '0',
            'box-shadow': '0 0 10px rgba(0, 0, 0, 0.1)'
          });
          $('main').css({
            'margin-top': '60px'
          });
        }
        if (($(window).scrollTop() > yOffset + 95) && (window.innerWidth > 768)) {
          $(".mainNav").css({
            'position': 'fixed',
            'top': '0',
            'left': '0',
            'box-shadow': '0 0 10px rgba(0, 0, 0, 0.1)'
          });
          $('main').css({
            'margin-top': '60px'
          });
        }
        if ((($(window).scrollTop() < yOffset + 95) && (window.innerWidth > 768)) || (($(window).scrollTop() < yOffset + 200) && (window.innerWidth < 768))) {
          $(".mainNav").css({
            position: 'relative',
            'box-shadow': 'none'
          });
          $('main').css({
            'margin-top': '0'
          });
        }
      });
    },

    // drawers
    myDrawers: function() {
      $(document).ready(function() {
        $('.cross-list li').click(function() {
          var elems = $('.cross-list-drawer');
          var elem = $('.cross-list-drawer#drawerId-' + $(this).attr('data-draw'));
          elems.slideUp("slow");
          elem.slideToggle();
          return false;
        });
      });
    },


    // download fix
    downloadBtnFix: function() {
      $(document).ready(function() {
        var ptitle = $(".wpdm-link-tpl").find(".ptitle");
        var link = $(".wpdm-download-link");
        ptitle.each(function(index) {
          var ptitleText = $(this).clone().children().remove().end().text();
          $(this).parent().find(".wpdm-download-link").text(ptitleText);
        });
      });
    }

  };

//Init App here
$(function() {
  App.init();
});
