<?php
if (!function_exists('theme_scripts')) {
	function theme_scripts() {
		$the_theme = wp_get_theme();

		// core
    wp_deregister_script('jquery');
		wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', array(), false, true);
    wp_enqueue_script('jquery-easing', '//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js', array('jquery'), null, false);
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/assets/main.min.js', array(), $the_theme->get('Version'), true);
		wp_enqueue_script('tether', '//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', array('jquery'), null, false);
		wp_enqueue_script('validate', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js', array('jquery'), null, false);
		wp_enqueue_script('validate-extra', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js', array('jquery'), null, false);

    // styles
		wp_enqueue_style('theme-css', get_stylesheet_directory_uri() . '/assets/main.min.css', array(), $the_theme->get('Version'));
	}
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );
