<?php

require get_template_directory() . '/assets/inc/enqueue.php';

// dashicons
function load_dashicons_front_end() {
  wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'load_dashicons_front_end');

// navwalker
require_once('wp_bootstrap_navwalker.php');

// menu
function register_ba_navigation() {
  $locations = array(
    'header-menu' => __('Header Menu', 'Demo'),
  );
  register_nav_menus( $locations );
}
add_action('init', 'register_ba_navigation');

// add class to menu item link
function add_specific_menu_location_atts( $atts, $item, $args ) {
    // check if the item is in the primary menu
    if( $args->theme_location == 'main_menu' ) {
      // add the desired attributes:
      $atts['class'] = 'navItem__link';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );

// pagination
function pagination($pages = '', $range = 4) {
  $showitems = ($range * 2)+1;
  global $paged;
  if(empty($paged)) $paged = 1;
  if($pages == '') {
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages) {
      $pages = 1;
    }
  }
  if(1 != $pages) {
    echo "<ul class=\"pagination\">";
    if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo; First</a></li>";
    if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a></li>";
    for ($i=1; $i <= $pages; $i++) {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
        echo ($paged == $i)? "<li class='active'><span class='current'>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
      }
    }
    if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a></li>";
    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>Last &raquo;</a></li>";
    echo "</ul>\n";
  }
}

// slug
function the_slug() {
  $post_data = get_post($post->ID, ARRAY_A);
  $slug = $post_data['post_name'];
  return $slug;
}

// lead
function first_paragraph($content)
{
  $page = get_query_var('page');
  if ($page < 2) {
    return preg_replace('/<p([^>]+)?>/', '<p$1 class="lead">', $content, 1);
  }
  else {
    return $content;
  }
}
add_filter('the_content', 'first_paragraph');

// thumbnails
if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 300, 300, true );
}
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'list', '150', '150', true );
  add_image_size( 'post', '200', '200', true );
  add_image_size( 'index', '300', '300', true );
  add_image_size( 'feat', '400', '480', true );
  add_image_size( 'album', '800', '600', true );
  add_image_size( 'slide', '1920', '480', true );
  add_image_size( 'service', '800', '400', true );
}

// excerpts
function youth_excerpt($length = '', $more = ''){
  global $post;
  if (function_exists($length)) {
    add_filter('excerpt_length', $length);
  }
  if (function_exists($more)) {
    add_filter('excerpt_more', $more);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}
function youth_excerpt_index($length) {
  return 50;
}
function youth_excerpt_report($length) {
  return 50;
}
function youth_excerpt_more($more) {
  return ' <a class="more-link" href="' . get_permalink($post->ID) . '">' . __('... Czytaj dalej &raquo;', '4youth') . '</a>';
}

// widget
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'before_widget' => '<ul><li>',
    'after_widget' => '</li></ul>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

// admin
show_admin_bar( false );
