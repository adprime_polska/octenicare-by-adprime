<?php /* Template Name: Index */ ?>
<?php get_header(); ?>

<main>
  <section class="mainSection mainSection__intro" id="intro">
    <div class="container">
      <div class="row v-center mb-3">
        <div class="col-3 ordnung-2">
          <article class="text-left">
            <span class="img-placement img-placement__logo"></span>

<?php query_posts('pagename=content'); if (have_posts) : the_post(); ?>
<?php the_field('intro_i'); ?>
<?php endif; wp_reset_query(); ?>

          </article>
        </div>
        <div class="col-6 ordnung-1">
          <span class="img-placement img-placement__infinity"></span>
          <span class="img-placement img-placement__infinityExtra"></span>
        </div>
        <div class="col-3 ordnung-3">
          <article class="text-right">
            <span class="img-placement img-placement__sticker"></span>
          </article>
        </div>
      </div>
      <?php echo do_shortcode("[wpdm_package id='94']"); ?>
    </div>
  </section>

  <section class="mainSection mainSection__zastosowanie" id="zastosowanie">
    <div class="container text-center">
      <h2 class="title">Zastosowanie</h2>
      <p class="mb-3">Wybierz kategorię, aby dowiedzieć się więcej.</p>
      <div class="row">
        <div class="col-4 locker__container">

<?php
$parent_id = get_cat_ID('Zastosowanie');
$args = array(
  'parent'        => $parent_id,
  'hide_empty'    => false,
  'number'        => 4,
  'offset'        => 0
);
foreach (get_categories($args) as $cat) : $date = get_field('date', $cat); ?>

          <a class="locker" href="<?php echo get_category_link($cat->term_id); ?>" title="<?php echo $cat->cat_name; ?>">
            <span class="locker__image" style="background-image: url('<?php echo z_taxonomy_image_url($cat->term_id); ?>');"></span>
            <h4 class="title title--brushed">
              <?php echo $cat->cat_name; ?>
            </h4>
            <h5 class="title title--subbed">
              <?php echo get_field('subtitle', $cat); ?>
            </h5>
          </a>

<?php endforeach; ?>

        </div>
        <div class="col-4 noHeight">
          <span class="img-placement img-placement__krem"></span>
        </div>
        <div class="col-4 locker__container">

<?php
$parent_id = get_cat_ID('Zastosowanie');
$args = array(
  'parent'        => $parent_id,
  'hide_empty'    => false,
  'number'        => 4,
  'offset'        => 4
);
foreach (get_categories($args) as $cat) : $date = get_field('date', $cat); ?>

          <a class="locker locker--inv" href="<?php echo get_category_link($cat->term_id); ?>" title="<?php echo $cat->cat_name; ?>">
            <span class="locker__image" style="background-image: url('<?php echo z_taxonomy_image_url($cat->term_id); ?>');"></span>
            <h4 class="title title--brushed">
              <?php echo $cat->cat_name; ?>
            </h4>
            <h5 class="title title--subbed">
              <?php echo get_field('subtitle', $cat); ?>
            </h5>
          </a>

<?php endforeach; ?>

        </div>
      </div>
      <a class="btn btn-none">Dowiedz się więcej</a>
    </div>
  </section>

  <section class="mainSection mainSection__zalety" id="zalety">
    <div class="container text-center">

<?php query_posts('pagename=content'); if (have_posts) : the_post(); ?>

      <h2 class="title">Moc składników</h2>
      <div class="row">
        <div class="col-4 ">
          <span class="img-placement img-placement__sample"></span>
          <span class="title title--caveat mt-1">Dostępny w aptekach!</span>
        </div>
        <div class="col-6">
          <article class="text-left">

<?php the_field('zalety'); ?>

          </article>
        </div>
        <div class="col-2 noHeight">
          <span class="img-placement img-placement__natural"></span>
        </div>
      </div>
      <a class="btn btn-none">Dowiedz się więcej</a>

<?php endif; wp_reset_query(); ?>

    </div>
  </section>

  <section class="mainSection mainSection__kontakt" id="kontakt">
    <div class="container text-center">
      <h2 class="title">Kontakt</h2>
      <div class="row">
        <div class="col-4">
          <span class="img-placement img-placement__infinity-ii"></span>
          <span class="title title--caveat text-center">Niezawodnie <span class="text-black">ile razy</span> <span class="text-pink">potrzebujesz</span></span>
        </div>
        <div class="col-8">
          <h4 class="title title--subtitle">
            Masz pytania? Odpowiemy na nie!
            <br>
            Zostaw kontakt.
          </h4>

<?php echo do_shortcode('[contact-form-7 id="35" title="Octenisept Contact Form"]'); ?>

        </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>
